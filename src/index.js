import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';

ReactDOM.render(React.createElement(App, null, null), document.getElementById('root'));

import {
  observable, decorate, computed, reaction,
} from 'mobx';

class MainStore {
  constructor() {
    if (localStorage.getItem('id') && localStorage.getItem('range')) {
      this.id.set(localStorage.getItem('id'));
      this.range.set(localStorage.getItem('range'));
      if (localStorage.getItem('rows')) {
        this.rows = JSON.parse(localStorage.getItem('rows'));
        this.nextWord('');
      }
    }
    localStorage.getItem('difficulty') && this.difficulty.set(localStorage.getItem('difficulty'));
    if (localStorage.getItem('maxScore')) {
      this.maxScore.set(+localStorage.getItem('maxScore'));
    }
  }

    originalWord = observable.box();

    translatedWord = observable.box();

    // [{wordEnd, wordRu, isCorrect}, {word, true/false}, ...]
    wordsBefore = observable([]);

    maxScore = observable.box(0);

    nowScore = observable.box(0);

    id = observable.box('1hSsPR0fN7I456-TZOUFJwOb7GjSrqeoOo02hMCy9NfI');

    range = observable.box('10,000 most common russian words');

    description = observable.box('');

    difficulty = observable.box(0.3);

    get encryptedWord() {
      const translatedWord = this.translatedWord.get();
      if (translatedWord === '...') return '...';
      if (translatedWord && translatedWord.length <= 2) return '__';
      if (translatedWord) {
        const splittedWord = translatedWord.split('');
        const isLetter = /[a-zA-Z]/;
        let mass = 0;
        const encryptedWord = splittedWord.map((value) => {
          if (!isLetter.test(value)) return value;
          if ((Math.random() - mass) < this.difficulty.get()) {
            mass = 0;
            return '_';
          }
          mass += (+this.difficulty.get()) / 2;
          return value;
        }).join('');
        return encryptedWord;
      }
      return '';
    }

apiKey = 'AIzaSyC0uJqAqTffOkM8xSp-BFgwTdxLZ7-zP7E';

get api() {
  return `https://sheets.googleapis.com/v4/spreadsheets/${this.id.get()}/values:batchGet?ranges=${this.range.get()}&majorDimension=ROWS&key=${this.apiKey}`;
}

fetchData() {
  fetch(this.api)
    .then(response => response.json())
    .then((data) => {
      this.rows = data.valueRanges[0].values;
      localStorage.setItem('rows', JSON.stringify(this.rows));
      localStorage.setItem('id', this.id.get());
      localStorage.setItem('range', this.range.get());
      this.nextWord('');
    });
}

nextWord(word) {
  if (this.rows) {
    if (word.toLowerCase()
    === (this.translatedWord.get() && this.translatedWord.get().toLowerCase())) {
      this.nowScore.set(this.nowScore.get() + 1);
      if (this.nowScore.get() > this.maxScore.get()) {
        this.maxScore.set(this.nowScore.get());
      }
      this.wordsBefore.push({
        originalWord: this.originalWord.get(),
        translatedWord: this.translatedWord.get(),
        correct: true,
      });
    } else {
      this.wordsBefore.push({
        originalWord: this.originalWord.get(),
        translatedWord: this.translatedWord.get(),
        correct: false,
      });
      this.nowScore.set(0);
    }

    const randomRow = Math.floor(Math.random() * this.rows.length);
    let translated = this.rows[randomRow][1];
    const separators = /[,;]/;
    const translatedSepareted = translated.split(separators);
    const hasRoundRracket = /(\().*(\))/;
    translated = translatedSepareted[Math.floor(Math.random() * translatedSepareted.length)];
    translated = translated.replace(hasRoundRracket, '').trim();

    this.originalWord.set(this.rows[randomRow][0]);
    this.translatedWord.set(translated);
    if (this.rows[randomRow][2]) {
      this.description.set(this.rows[randomRow][2].split('|')[0]);
    }
  }
}
}

decorate(MainStore, {
  encryptedWord: computed,
  api: computed,
});


const mainStore = new MainStore();

reaction(
  () => mainStore.difficulty.get(),
  (difficulty) => { localStorage.setItem('difficulty', difficulty); },
);
reaction(
  () => mainStore.maxScore.get(),
  (maxScore) => { localStorage.setItem('maxScore', maxScore); },
);
reaction(
  () => mainStore.wordsBefore.length,
  (length) => {
    if (length > 12) {
      mainStore.wordsBefore.shift();
    }
  },
);

export default mainStore;

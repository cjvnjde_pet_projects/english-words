import styled from 'styled-components';

export default styled.div`
    font-size: 2rem;
    margin-bottom: .5rem;
    margin-top: .2rem;
`;

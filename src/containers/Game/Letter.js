import styled from 'styled-components';

export default styled.span`
    background-color: ${props => (props.correct ? '#b3f9ae' : '#f46e61')}
`;

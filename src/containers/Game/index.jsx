import React from 'react';
import { observer } from 'mobx-react';

import GameContainer from './GameContainer';
import WordToTranslate from './WordToTranslate';
import WordInput from './WordInput';
import WordParts from './WordParts';
import Letter from './Letter';

import mainStore from '../../stores/mainStore';

class Game extends React.Component {
  state = {
    value: '',
  }


  handleChange = (e) => {
    this.setState({
      value: e.target.value,
    });
  }

  handleKeyPress = (e) => {
    const { value } = this.state;
    if (e.key === 'Enter') {
      mainStore.nextWord(value);
      this.setState({
        value: '',
      });
    }
  }

  wrapEncrypted = word => word.split('').map((letter, index) => {
    const { value } = this.state;
    if (letter) {
      if (value[index]
        && ((letter === '_')
        || (letter.toLowerCase() === value[index].toLowerCase()))) {
        return (<Letter correct key={index}>{letter}</Letter>);
      }
      return (<Letter correct={false} key={index}>{letter}</Letter>);
    }
    return null;
  })

  render() {
    const { value } = this.state;

    return (
      <GameContainer>
        <WordToTranslate>
          {mainStore.originalWord.get()}
        </WordToTranslate>
        <WordInput
          value={value}
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
        />
        <WordParts letters={value.length}>
          {this.wrapEncrypted(mainStore.encryptedWord)}
        </WordParts>
      </GameContainer>
    );
  }
}

export default observer(Game);

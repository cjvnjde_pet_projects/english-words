import styled from 'styled-components';

export default styled.input`
    font-size: 2rem;
    text-align: center;
`;

import styled from 'styled-components';

export default styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    border-top: 2px solid blue;
    border-bottom: 2px solid blue;
`;

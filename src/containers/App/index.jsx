import React from 'react';
import { observer } from 'mobx-react';

import Header from '../../components/Header';
import Statistic from '../../components/Statistic';
import Game from '../Game';
// import WordDescription from '../../components/WordDescription';
import WordsBefore from '../../components/WordsBefore';
import Settings from '../../components/Settings';

import mainStore from '../../stores/mainStore';

const App = () => (
  <div className="App">
    <Header
      id={mainStore.id.get()}
      handleChangeId={(e) => {
        mainStore.id.set(e.target.value);
      }}
      range={mainStore.range.get()}
      handleChangeRange={(e) => {
        mainStore.range.set(e.target.value);
      }}
      fetchData={(e) => { mainStore.fetchData(); }}
    />
    <Statistic
      now={mainStore.nowScore.get()}
      max={mainStore.maxScore.get()}
    />
    <Game />
    {/* <WordDescription
        description={mainStore.description.get()}
      /> */}
    <Settings
      difficulty={mainStore.difficulty.get()}
      handleChange={(e) => { mainStore.difficulty.set(e.target.value); }}
    />
    <WordsBefore wordsBefore={mainStore.wordsBefore.toJS()} />

  </div>
);
export default observer(App);

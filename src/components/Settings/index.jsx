import React from 'react';

import DifficultyInput from './DifficultyInput';

// make it later
const Settings = ({ difficulty, handleChange }) => (
  <div>
      difficulty:
    {' '}
    <DifficultyInput type="text" value={difficulty} onChange={handleChange} />
  </div>
);

export default Settings;

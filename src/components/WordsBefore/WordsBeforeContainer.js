import styled from 'styled-components';

export default styled.div`
    display: flex;
    flex-direction: column-reverse;
    justify-content: space-around;
    align-items: center;
    flex-wrap: wrap;
    margin-top: 2.3rem;
`;

import styled from 'styled-components';

export default styled.div`
    background-color: ${props => (props.correct === true ? '#b3f9ae' : '#f46e61')};
    font-size: 1.5rem;
    margin: 0.5rem;
    /* width: 30rem; */
    text-align: center;
    display: flex;
    justify-content:  space-between;
    div {
        margin: 0 1rem;
    }
`;

import React from 'react';
import PropTypes from 'prop-types';
import WordsBeforeContainer from './WordsBeforeContainer';
import WordBefore from './WordBefore';

const WordsBefore = ({ wordsBefore }) => {
  const wordsBwforeList = wordsBefore.map((value) => {
    if (value.originalWord === undefined) return null;
    return (
      <WordBefore key={value.originalWord + value.translatedWord} correct={value.correct}>
        <div>
          {value.originalWord}
        </div>
        <div>
          {value.translatedWord}
        </div>
      </WordBefore>);
  });
  return (
    <WordsBeforeContainer>
      {wordsBwforeList}

    </WordsBeforeContainer>
  );
};

WordsBefore.propTypes = {
  wordsBefore: PropTypes.arrayOf(PropTypes.shape({
    originalWord: PropTypes.string,
    translatedWord: PropTypes.string,
    correct: PropTypes.bool,
  })).isRequired,
};

export default WordsBefore;

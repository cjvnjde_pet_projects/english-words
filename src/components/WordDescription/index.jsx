import React from 'react';
import PropTypes from 'prop-types';

import Description from './Description';

const WordDescription = ({ description }) => (
  <Description>
    {description}
  </Description>
);

WordDescription.propTypes = {
  description: PropTypes.string,
};

WordDescription.defaultProps = {
  description: '',
};

export default WordDescription;

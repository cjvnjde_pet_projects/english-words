import styled from 'styled-components';

export default styled.div`
    text-align: center;
    margin: 1rem;
    font-size: 1.5rem;
`;

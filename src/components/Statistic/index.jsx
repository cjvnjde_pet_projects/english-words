import React from 'react';
import PropTypes from 'prop-types';

import StatisticContainer from './StatisticContainer';

const Statistic = ({ now, max }) => (
  <StatisticContainer>
    <div>
      {`score: ${now}`}
    </div>
    <div>
      {`max score: ${max}`}
    </div>
  </StatisticContainer>
);

Statistic.propTypes = {
  now: PropTypes.number,
  max: PropTypes.number,
};

Statistic.defaultProps = {
  now: 0,
  max: 0,
};

export default Statistic;

import styled from 'styled-components';

export default styled.input`
    height: 1.5rem;
    width: 40%;
    min-width: 10rem;
    margin: 1rem;
    text-align: center;
    @media only screen and (max-width: 600px) {
        width: calc(100% - 2rem);
    }
`;

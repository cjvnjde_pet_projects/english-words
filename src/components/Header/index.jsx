import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import SpreadsheetId from './SpreadsheetId';
import Range from './Range';
import FetchDataBtn from './FetchDataBtn';

const AppHeader = ({
  id, handleChangeId, range, handleChangeRange, fetchData,
}) => (
  <Header>
    <SpreadsheetId
      value={id}
      onChange={handleChangeId}
    />
    <FetchDataBtn
      onClick={fetchData}
    >
    Fetch
    </FetchDataBtn>
    <Range
      value={range}
      onChange={handleChangeRange}
    />
  </Header>
);

AppHeader.propTypes = {
  id: PropTypes.string,
  handleChangeId: PropTypes.func.isRequired,
  range: PropTypes.string,
  handleChangeRange: PropTypes.func.isRequired,
  fetchData: PropTypes.func.isRequired,
};

AppHeader.defaultProps = {
  id: '',
  range: '',
};

export default AppHeader;

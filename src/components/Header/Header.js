import styled from 'styled-components';


export default styled.header`
    display: flex;
    background-color: blue;
    justify-content: space-around;
    align-items: center;
    flex-wrap: nowrap;

    @media only screen and (max-width: 600px) {
        flex-direction: column;
    }
`;

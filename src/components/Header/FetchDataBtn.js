import styled from 'styled-components';

export default styled.button`
    width: 5rem;
    height: 2rem;
    /* padding-bottom: 2rem; */
    border-radius: 0.1rem;
    overflow: hidden;
    background: #e0e0fc;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 1rem;
    :hover {
    background: #b3b3ff;
}
    @media only screen and (max-width: 600px) {
        order: 3;
        margin: 0;
        margin-bottom: 1rem;
    }
`;

# english words
Simple app for vocabulary training.

[live demo](https://cjvnjde_pet_projects.gitlab.io/english-words/)

desktop view

![](https://monosnap.com/image/Kc8jNhyloe5wfGOp77nCo3aZHUXudA.png)

mobile view

![](https://monosnap.com/image/c5Ds9bghvPevKlia9HvLm8sITdUy7r.png)

You can set your own dictionary:
1. create google sheet
2. add words (column A: ru, column B: eng)
3. create sharing link
4. copy sheet id (in link .../d/[id]/edit...)
5. copy sheet name
6. fetch data

The following libraries were used:
- react
- mobx
- styled-components

Also used google sheet api for fetching data.